FROM rocker/tidyverse

RUN apt-get update -qq

RUN apt-get install apt-utils -y

RUN apt-get -y --no-install-recommends install \
  apt-transport-https \
  libssl-dev \
  libsasl2-dev \
  openssl \
  curl \
  unixodbc \
  unixodbc-dev odbcinst \
  gnupg \
  libc6 libc6-dev libc6-dbg

RUN apt-get install unixodbc unixodbc-dev --install-suggests -y

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update -qq
RUN ACCEPT_EULA=Y apt-get install msodbcsql17
RUN ACCEPT_EULA=Y apt-get install mssql-tools
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc

RUN R -e "install.packages('odbc', repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('shiny', repos='http://cran.rstudio.com/')"

RUN R -e "library(odbc)"
RUN R -e "library(shiny)"

# After you start the container, open it on port 8787, the default user/pwd is rstudio
